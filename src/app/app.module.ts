import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { ParkingService } from 'src/services/parking.service';
import { HeaderComponent } from './layout/header/header.component';
import { DataTableComponent } from './layout/data-table/data-table.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { InputFormComponent } from './layout/input-form/input-form.component';
import { SharedService } from './shared/shared.service';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    DataTableComponent,
    InputFormComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule,
    NgbModule
  ],
  providers: [ParkingService, SharedService],
  bootstrap: [AppComponent]
})
export class AppModule { }
