import { Component, OnInit, OnDestroy, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { Subscription } from 'rxjs';
import { ParkingService } from 'src/services/parking.service';
import { SharedService } from 'src/app/shared/shared.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit, OnDestroy {

  searchSubscription$: Subscription;


  form: FormGroup;

  searchVia = [
    { value: 'registrationNo', label: 'Registration No' },
    // { value: 'Slot', label: 'Slot' }
  ];

  constructor(
    private formBuilder: FormBuilder,
    private _parkingService: ParkingService,
    private _sharedService: SharedService,

  ) { }

  ngOnInit(): void {
    this.form = this.formBuilder.group({
      registrationNo: new FormControl(null, Validators.required),
      searchVia: new FormControl('registrationNo', Validators.required),
    });
  }

  onSubmit() {
    this.searchSubscription$ = this._parkingService.searchByRegNo(this.form.value).subscribe(
      (res) => {
        if (res.status) {
          let msg = `${res.data.vehicle[0]['registrationNo']} is parked at :  Level ${res.data.result[0]['levelId']}/ Row ${res.data.result[0]['rowId']}/ Slot No :${res.data.result[0]['slotNumber']}`
          this._sharedService.sendMessage({ status: "success", message: msg });
        } else {
          this._sharedService.sendMessage({ status: "error", message: res.message });
        }
      }, (err) => {
        this._sharedService.sendMessage({ status: "error", message: 'Vehicle not found' });
      }
    );
  }

  ngOnDestroy() {
    if (this.searchSubscription$) {
      this.searchSubscription$.unsubscribe();
    }
  }

}
