import { Component, OnInit, OnDestroy, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { isGeneratedFile } from '@angular/compiler/src/aot/util';
import { ParkingService } from 'src/services/parking.service';
import { SharedService } from 'src/app/shared/shared.service';

@Component({
  selector: 'app-input-form',
  templateUrl: './input-form.component.html',
  styleUrls: ['./input-form.component.scss']
})
export class InputFormComponent implements OnInit, OnDestroy {

  @Output() updateParkedVehicles = new EventEmitter<boolean>();

  getSlotSubscription$: Subscription;
  form: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private _parkingService: ParkingService,
    private _sharedService: SharedService,

  ) { }

  ngOnInit(): void {
    this.form = this.formBuilder.group({
      vehicleType: [null, Validators.required],
      registrationNo: new FormControl(null, Validators.required),
      noOfSlots: new FormControl(null)
    });
  }
  message: string = '';

  onSubmit() {
    this.form.value['noOfSlots'] = this.form.value['vehicleType'] === 'L' ? 5 : 1;
    this.getSlotSubscription$ = this._parkingService.getParkingSlot(this.form.value).subscribe(
      (res) => {
        if (res.status) {
          this.message = res.data.message;
          this.form.reset();
          this.updateParkedVehicles.emit(true);
          this._sharedService.sendMessage({ status: "success", message: res.message });
        } else {
          this._sharedService.sendMessage({ status: "error", message: res.message });
        }
      },
      (err) => {
        this._sharedService.sendMessage({ status: "error", message: err.error.message });
      }
    );
  }

  ngOnDestroy() {
    if (this.getSlotSubscription$) {
      this.getSlotSubscription$.unsubscribe();
    }
  }

}
