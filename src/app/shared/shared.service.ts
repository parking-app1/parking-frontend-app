import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SharedService {

  private alertMessageSource = new BehaviorSubject<any>(null);

  constructor() { }

  sendMessage(data: any) {

    this.alertMessageSource.next(data);
  }
  getMessage(): Observable<any> {
    return this.alertMessageSource.asObservable();
  }


}
