import { Component, OnInit, OnDestroy } from '@angular/core';
import { ParkingService } from 'src/services/parking.service';
import { Subscription, Subject, } from 'rxjs';
import { debounceTime } from 'rxjs/operators';

import { SharedService } from './shared/shared.service';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy {
  title = 'parking-frontend';

  getParkingSlotSubscription$: Subscription;
  getParkedVehiclesSubscription$: Subscription;

  message: string = '';
  data: any = [];
  alertType: string = '';

  private _success = new Subject<string>();
  successMessage: string;
  constructor(
    private _parkingService: ParkingService,
    private _sharedService: SharedService,

  ) {

  }
  ngOnInit() {
    this.getAllCount();

    this._success.subscribe((message) => this.successMessage = message
    );
    this._success.pipe(
      debounceTime(5000)
    ).subscribe(() => this.successMessage = null);
    this._sharedService.getMessage().subscribe(result => {
      if (result) {
        if (result.status == "warning") {
          this.alertType = 'warning';
        } else if (result.status == "success") {
          this.alertType = 'success';
        } else {
          this.alertType = 'danger';
        }
        this._success.next(result.message);
      }
    });


  }

  arr = [
    { key: 'first', isActive: true },
    { key: 'second', isActive: false },
    { key: 'third', isActive: false }
  ];

  getParkingSlot() {
    let payload = {
      registrationNo: 'MH12-XXXXX',
      vehicleType: 'Single'
    }
    this.getParkingSlotSubscription$ = this._parkingService.getParkingSlot(payload).subscribe(
      (res) => {
        if (res['status']) {
          this.message = res.data.key;
          this.getAllCount();
        }
      }, (err) => { }
    );
  }


  getAllCount() {
    this.getParkingSlotSubscription$ = this._parkingService.getAllCount().subscribe(
      (res) => {
        this.data = [];
        this.data = res.data.result;
      }, (err) => { }
    );
  }

  getSearchedData(data) {
    this.data = [];
    this.data = data;
  }
  close() {

  }

  ngOnDestroy() {
    if (this.getParkingSlotSubscription$) {
      this.getParkingSlotSubscription$.unsubscribe();
    } if (this.getParkingSlotSubscription$) {
      this.getParkingSlotSubscription$.unsubscribe();
    }
  }
}
