import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': "application/json" })
};


@Injectable({
  providedIn: 'root'
})
export class ParkingService {

  constructor(
    private http: HttpClient
  ) { }

  // to search vehicle using registration number
  searchByRegNo(payload: object) {
    return this.http.post<any>(`${environment.baseUrl}parkedVehicles/search`, payload);
  }

  getParkingSlot(payload) {
    return this.http.post<any>(`${environment.baseUrl}parkedVehicles/getParkingSlot`, payload);
  }


  // to get count of each kind of vehicle
  getAllCount() {
    return this.http.get<any>(`${environment.baseUrl}parkedVehicles/getAllCount`);
  }



}
