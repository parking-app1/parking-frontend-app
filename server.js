const express = require('express');

const app = express();

app.use(express.static('./dist/parking-frontend'));


app.get('/*', (req, res) =>
    res.sendFile('index.html', { root: 'dist/parking-frontend/' }),
    // res.sendFile('index.html', { root: 'src/app/' }),
);
console.log(`Angular app running`)
app.listen(process.env.PORT || 9000);
